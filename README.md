# Python_WaterResources_2019

This repository was created to host the Python course for geospatial analysis, taught at UNALM, to have an iterative methodology and open use of the modules required for it.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/cenciso%2Fpython_waterresources_2019/master)